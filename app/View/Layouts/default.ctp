<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 */
$cakeDescription = __d('cake_dev', 'Atrakcje Świata, czego chcieć więcej');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>



<!DOCTYPE html>
<html>
    <head>

<?php echo $this->Html->charset(); ?>
        <title>
        <?php echo $cakeDescription ?>:
            <?php echo $this->fetch('title'); ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<?php
echo $this->Html->meta(
        'favicon.ico', '/favicon.ico', array('type' => 'icon')
);

echo $this->Html->css('cake.generic');

echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->Html->css('bootstrap');

echo $this->fetch('script');
?>


        <script src="http://localhost/projekt/js/bootstrap.js"></script>
        
        <script type="text/javascript">
        $(document).ready(function(){
            $("#big").click(function (){
              $("#content").css('font-size', "20pt") 
            });
            $("#sml").click(function (){
              $("#content").css('font-size', "") 
            });
            var flag = false;
            $("#hc").click(function (){
              if(flag == true){
                  flag=false;
                  
                $(".loginLogoutBox").css('background', "black")
                $(".loginLogoutBox").css("color", "yellow")
                $(".roundBox").css('background', "black")
                $(".roundBox").css("color", "yellow")
                $("#header").css('background', "black")
                $("#header").css("color", "yellow")
                $(".btn").css('background', "black")
                $(".btn").css("color", "yellow")
                $("tr").css('background', "black")
            }else{
                flag=true;
                $(".loginLogoutBox").css('background', "")
                $(".loginLogoutBox").css("color", "")
                $(".roundBox").css('background', "")
                $(".roundBox").css("color", "")
                $("#header").css('background', "")
                $("#header").css("color", "")
                $(".btn").css('background', "")
                $(".btn").css("color", "")
                $("tr").css('background', "")
            }
            });
        });
        </script>
        
    </head>
    <body>
        <div id="container">
            <div style="height: 45px; width: 140px; background: #cccccc; position: absolute; right: 30px; top: 120px; border-radius: 0px 0px 10px 10px; padding: 0px; z-index: 999999;">
                    <button id='big' style="color: #333333; font-size: 14pt; margin-left: 5px; margin-top: 5px; border-radius: 5px; width: 40px;">A+</button>
                    <button id='sml' style="color: #333333; font-size: 14pt; border-radius: 5px; width: 40px;">A-</button>
                    <button id='hc' style="color: #333333; font-size: 14pt; border-radius: 5px; width: 40px;">C</button>
                </div>
            <div id="header" class="hidden-sm hidden-xs">

                <div class="col-sm-3 col-md-3 col-lg-3">
                    <div id="logo"></div>
                </div>
                <div class="hidden-xs col-sm-6 col-md-5 col-lg-6 myMenu">
                    <a class="btn btn-warning" href="/projekt/users/add">Zapisz się</a>
                    <a class="btn btn-link" href="/projekt/places/index">Eksploruj</a>
                    <a class="btn btn-link" href="/projekt/places/add">Dodaj</a>
                </div>
                <div class="hidden-xs col-sm-3 col-md-3 col-lg-3 loginLogoutBox" style="color: grey;">
                        
                        <?php if ($logged_in): ?>
                                            zalogowany jako <?php echo $current_user['username'] ?> 
                        <?php
                        echo $this->Html->link('wyloguj ', array('controller' => 'users', 'action' => 'logout'));
                        ?>
                        <?php else: ?>
                        <?php
                        echo $this->Html->link('zaloguj się ', array('controller' => 'users', 'action' => 'login'))
                        ?>
                    <?php endif; ?>
                </div>

            </div>
            
             <div class="mobileContainer visible-sm visible-xs">
        <nav class="navbar navbar-default navbar-fixed-top">
      
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mobile">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top"><img src="http://localhost/projekt/img/logo.png" style="margin-top: -10px; height: 40px;"/></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
           <div class="collapse navbar-collapse" id="mobile">
             <div class="mobile-menu" id="mobile">
                    <a href="/projekt/users/add" class="mobile-item">Zapisz się</a>
                    <a href="/projekt/places/index" class="mobile-item">Eksploruj</a>
                    <a href="/projekt/places/add" class="mobile-item">Dodaj</a>
                    <a><hr></a>
                    <a>
                        
                                            <?php if ($logged_in): ?>
                                            zalogowany jako <?php echo $current_user['username'] ?> 
                                            <br><br>
                        <?php
                        echo $this->Html->link('wyloguj ', array('controller' => 'users', 'action' => 'logout'), array('class' => 'btn btn-warning'));
                        ?>
                        <?php else: ?>
                        <?php
                        echo $this->Html->link('zaloguj się ', array('controller' => 'users', 'action' => 'login'),
						array('class' => 'btn btn-warning'))
                        ?>
                    <?php endif; ?>
                                            <br><br>
                    </a>

            </div>
            </div>
    </nav>
             </div>
            <div class="container-fluid ">
                <div id="content">

                    <br />
                    <?php
                    if (isset($myName)) {
                        echo "<div class='roundBox col-lg-8 col-lg-offset-2'>";
                    } else {
                        //echo 'nie jest';
                    }
                    ?>
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->Session->flash('auth'); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
                <div id="footer">

                </div>
            </div>


    </body>
</html>
