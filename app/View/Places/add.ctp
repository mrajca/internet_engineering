<div class="places form">
<!-- część odpowiedzialna za wyświetlanie formularza -->
<?php echo $this->Form->create('Place',array('enctype'=>'multipart/form-data')); ?>
			<br>
	<fieldset>

		<legend><?php echo __('Dodaj miejsce'); ?></legend>
	<?php
		//class=>form-control umożliwa stylizację poprawnie formularz
		echo $this->Form->input('name', array('label' => 'Nazwa atrakcji', 'class'=>'form-control'));
		echo $this->Form->input('description', array('label' => 'Opis', 'class'=>'form-control'));
		echo $this->Form->input('city', array('label' => 'Miasto', 'class'=>'form-control'));
		echo $this->Form->input('country_id', array('label' => 'Kraj', 'class'=>'form-control'));
		echo $this->Form->input('image', array('label' => 'Adres URL obrazka', 'class'=>'form-control'));
		echo $this->Form->input('author', array('value'=>@$current_user[username], 'style'=>'display:none', 'label' => false, 'class'=>'form-control')); // dodawanie nazwy użykownika w tle, niewidoczne w formularzu current user ustawia nazwę
		
	?>
			 
	</fieldset>
	<br />
<input value="dodaj" class="btn btn-primary" type="submit">
<br><br>
</form>
</div>
<div class="actions"  <?php if($current_user['role'] != 'admin') echo ("style=\"display:none;\"")?>>
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Places'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Comments'), array('controller' => 'comments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Comment'), array('controller' => 'comments', 'action' => 'add')); ?> </li>
	</ul>
</div>
