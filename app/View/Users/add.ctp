<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
	 <div class="form-group">
	 	<br />
		<legend><?php echo __('dodaj podróżnika'); ?></legend>
	
		<label for="exampleInputEmail1">login:</label>
		<input class="form-control" name="data[User][username]" maxlength="150" id="UserUsername" required="required" type="text">
		
		<label for="exampleInputEmail1">e-mail:</label>
		<input class="form-control"  name="data[User][email]" maxlength="150" id="UserEmail" required="required" type="email">
		
		<label for="exampleInputEmail1">imie i nazwisko:</label>
		<input class="form-control"  name="data[User][name]" maxlength="100" id="UserName" required="required" type="text">
		
		<label for="exampleInputEmail1">hasło:</label>
		<input class="form-control"  name="data[User][password]" id="UserPassword" required="required" type="password">
		
		<label for="exampleInputEmail1">powtórz hasło:</label>
		<input class="form-control"  name="data[User][password_confirm]" id="UserPasswordConfirm" required="required" type="password">
		
	</fieldset>
<input value="dodaj" class="btn btn-primary" type="submit">
</form>
<br />
</div>
</div>
