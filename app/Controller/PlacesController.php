<?php
App::uses('AppController', 'Controller');
/**
 * Places Controller
 *
 * @property Place $Place
 * @property PaginatorComponent $Paginator
 */
class PlacesController extends AppController { //klasa atrakcji, obsługiwane jest tu całe CRUD miejsc

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() { // służy do wyświetlania listy atrakcji
		if($this->request->is('post')){ //moduł wyszukiwarki
			$search = $this->request->data['Search']['tag']; //wybranie z data tego co będzie kluczem poszukiwań
			$this->paginate = array('conditions'=>array('OR' => array(array('place.name LIKE'=>'%'.$search.'%'), array('place.city LIKE'=>'%'.$search.'%')))); // warunki wyszukiwania można dodawać inne, obecnie tylko miasto i nazwa
			$this->Session->setFlash(__('Wyniki wyszukiwania:'));
		}
		$this->set('attractions', $this->Paginator->paginate());
		$this->set('myName', 'myTest');//zmienna pomocnicza, decydująca jaki css ma być załadowany, dzięki temu że jeste ustawiona dodolną treścią, wszystko wyświetla się w środku głównego pola
		$this->Place->recursive = 0;
		$this->set('places', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) { //wyświetlanie wybranej atracji
		$this->set('myName', 'myTest');//podobnie jak w pierwszym wystąpieniu
		if (!$this->Place->exists($id)) {
			throw new NotFoundException(__('Invalid place'));
		}
		$options = array('conditions' => array('Place.' . $this->Place->primaryKey => $id)); //wskazanie co ma być wyświetlane dzięki ID
		$this->set('place', $this->Place->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() { //dodawanie atrakcji
		$this->set('myName', 'myTest');//jak poprzednio
		if ($this->request->is('post')) {//wypełnianie pól
			$this->set('myName', 'myTest');
			$this->Place->create();//dodanie rekordu do bazy
			if ($this->Place->save($this->request->data)) {
				$this->Session->setFlash(__('The place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The place could not be saved. Please, try again.'));
			}
		}
		$countries = $this->Place->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {//edycja
		$this->set('myName', 'myTest');
		if (!$this->Place->exists($id)) {
			throw new NotFoundException(__('Invalid place'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Place->save($this->request->data)) {
				$this->Session->setFlash(__('The place has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The place could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Place.' . $this->Place->primaryKey => $id));
			$this->request->data = $this->Place->find('first', $options);
		}
		$countries = $this->Place->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {//usuwanie miejsca
		$this->set('myName', 'myTest');
		$this->Place->id = $id;
		if (!$this->Place->exists()) {
			throw new NotFoundException(__('Invalid place'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Place->delete()) {
			$this->Session->setFlash(__('The place has been deleted.'));
		} else {
			$this->Session->setFlash(__('The place could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function xxx() {
            
	}
}
