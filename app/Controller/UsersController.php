<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
                $this->set('myName', 'asd');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
            $this->set('myName', 'myTest');
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
            $this->set('myName', 'myTest');
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('dodano użytkownika.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('nie udało się zapisać użytkownika, spróbuj ponownie.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
            $this->set('myName', 'myTest');
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('dodano użytkownika.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('nie udało się zapisać użytkownika, spróbuj ponownie.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('usunięto użytkownika'));
		} else {
			$this->Session->setFlash(__('nie można usunąć, spórbuj ponownie.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function beforeFilter() {
		parent::beforeFilter();
		// Allow users to register and logout.
		$this->Auth->allow('add', 'logout');
	}
	
	public function isAuthorized($user){
		if (in_array($this->action,array('edit','delete'))){
			if($user['role']=='admin'){
				return true;
			}
			
			if($user['id'] != $this->request->params['pass'][0]){
				
				return false;
			}
		}
		return true;
	}
	
	public function login() {
		$this->set('myName', 'myTest');
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirectUrl('/places/index'));
			}
			$this->Session->setFlash(__('błędna nazwa użytkownika lub hasło'));
		}
	}
	
	public function logout() {
		return $this->redirect($this->Auth->logout());
	}

	public function welcome() {
            //$this->set('myName', 'myTest');
            //$this->set('myName', 'asd');
	}
	
}
